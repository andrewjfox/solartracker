/* Solar Tracker for scheffler dish
  CERES project
  
  Controller reads sun sensor - differential input
  drives motor via motor driver

  each sensor input is 0 - 10V, then /2 voltage divider
  so fullscale counts 1023 = 10V, and 1V = 102 counts 

  8/4/16 - monitor output control pins, will be pulled low if limit switch active
        - if cloud cover, limp in tracking direction to minimise time to get back on sun 
        - return to home position when tracking limit reached
  
 */
#include <EEPROM.h> 
#include "ard_sysvar.h"

#define BUFF_SIZE  100

#define SHORT_INTERVAL  1000
#define LONG_INTERVAL   10000

char cmdStr[BUFF_SIZE + 1];
int rxCnt;

int InputA = A0; 
int InputB = A1;
int OutputA = 6;
int OutputB = 7;

unsigned long sample_timer;
unsigned long sample_period = LONG_INTERVAL;

int error;
int error_limit;


void setup() {                
  sysvar_init();
  attach_channel_read(io_rd_func);
  attach_channel_write(io_wr_func);  
  
  // debug output
  Serial.begin(19200);

  Stop();
  sample_timer = millis();
}



void loop() 
{ 
 int sensorA, sensorB; 
 int state = 0;
 unsigned int cloudCntr = 0;

 sample_period = LONG_INTERVAL;
 
 while(1) {   
   if((millis() - sample_timer) > sample_period) {
    sample_timer = millis();  
    sample_period = LONG_INTERVAL;
    
    sensorA = analogRead(InputA);
    sensorB = analogRead(InputB);
    
    Serial.print(state);   
    Serial.print(", ");   
    Serial.print(sensorA);   
    Serial.print(", ");   
    Serial.print(sensorB);   
    Serial.print(", ");   
    
    switch(state) {
      case 0: //IDLE
              if(SunlightOk()) {
                state = 1;
                Serial.print("Sun Detect, Start Tracking");
                cloudCntr = 0;
              }
              break;
      
      case 1: //TRACKING                
                error_limit = sysvar_read(SYSVAR_ERR_LIMIT_IDX)->i;
                error = (SunlightOk()) ? (sensorA - sensorB):0;
                if(cloudCntr > sysvar_read(SYSVAR_CLOUD_THRESH_IDX)->i) {
                  error = -error_limit-1;
                }
                if(error > error_limit) {   // home direction
                   if(goHome())
                    Serial.print("Home");
                   else {
                    Serial.print("Home Lim");
                    state = 0;
                   }
                   cloudCntr = 0;
                   sample_period = SHORT_INTERVAL;
                 } else if(error < -error_limit) {  // tracking direction
                    if(goTracking())
                      Serial.print("Tracking");
                   else {
                      Serial.print("Tracking Lim");
                      state = 2;
                   }  
                   cloudCntr = 0;
                   sample_period = SHORT_INTERVAL;
                 } else {
                   Stop();
                   Serial.print("Stop");
                   if(!SunlightOk()) {
                    // lost sun, continue limp mode tracking
                    Serial.print("Cloud Counter = ");
                    Serial.print(++cloudCntr);
                   }
                 }                                  
              break;
              
      case 2: // HOMING
             if(goHome())
              Serial.print("Homing");
             else {
                Stop();
                Serial.print("Home Lim"); 
                state = 0;
             } 
             break; 
             
      default:
              state = 0;
              break;
      }
      Serial.println(); 
   }
   
   // check command interface
   if (Serial.available()) {
      char inChar = (char)Serial.read();
      Serial.write(inChar); // echo
      rxCnt = appendChar(inChar, cmdStr, rxCnt, BUFF_SIZE);
      // local command handler
      if (inChar == '\r') {
        if ((rxCnt > 1) && (processCmd(cmdStr))) {
          Serial.print(cmdStr);
        }
        cmdStr[0] = 0;
        rxCnt = 0;
      }
    }
        
 } // while(1)
   
}  // loop()



boolean SunlightOk(void) {
  if((analogRead(InputA) > sysvar_read(SYSVAR_SUN_THRESH_IDX)->i) || (analogRead(InputB) > sysvar_read(SYSVAR_SUN_THRESH_IDX)->i)) 
    return true;
  else
    return false;    
}


boolean Stop(void)
{
  pinMode(OutputA,OUTPUT);
  pinMode(OutputB,OUTPUT);
  digitalWrite(OutputA,LOW);
  digitalWrite(OutputB,LOW);

  return true;
}  


boolean goHome(void)
//boolean Forward(void)
{
  int pinState;
  
  pinMode(OutputA,INPUT_PULLUP);
  digitalWrite(OutputB,LOW);    
  delay(10);
  pinState = digitalRead(OutputA);

  if(pinState == HIGH) {
    pinMode(OutputA,OUTPUT);
    digitalWrite(OutputA,HIGH);
  }

  return (pinState == HIGH) ? true:false;
}  


boolean goTracking(void)
//boolean Reverse(void)
{
  int pinState;
  
  pinMode(OutputB,INPUT_PULLUP);
  digitalWrite(OutputA,LOW);    
  delay(10);
  pinState = digitalRead(OutputB);

  if(pinState == HIGH) {
    pinMode(OutputB,OUTPUT);
    digitalWrite(OutputB,HIGH);
  }

  return (pinState == HIGH) ? true:false;
}

/*************************************************
*  io read and write functions for protocol handler
*/
#define IO_CH_ANA_MIN  0
#define IO_CH_ANA_MAX  3

#define ADC_FS_MV  5000
#define ADC_FS_COUNTS  1023

VarRecord_t* io_rd_func(uint16 idx)
{
  if((idx >= IO_CH_ANA_MIN) && (idx <= IO_CH_ANA_MAX)) {
    globalVarRecord.type = sysvar_decimal;
    globalVarRecord.i = map(analogRead(A0 + idx), 0, ADC_FS_COUNTS, 0, ADC_FS_MV);
  }
  else {
    globalVarRecord.type = sysvar_none;
    globalVarRecord.i = 0;
  }
  
  return &globalVarRecord; 
}


int io_wr_func(uint16 idx, VarRecord_t* VarRecord)
{
  return 1;
}

/****  buffer handling helper function
 *   assemble input string, ready to pass to command handler
 */
int appendChar(char inChar, char *buff, int count, int buffSize)
{
    if (count >= buffSize) count = buffSize - 1;
    if (inChar == 0x08)  { // backspace
      if (count) --count;
      buff[count] = 0;
    }
    else {
      buff[count++] = inChar;
      buff[count] = 0;
    }

  return count;
}

// end of file  
