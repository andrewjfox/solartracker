/********************************************
 system variables for arduino target
 
 Andrew Fox - Flair Engineering Pty Ltd 
 Sep 2014
 
 includes protocol handler (transport layer independant) to read/write variables and channels
 
 supports discrete, decimal (integer) and string types
 
 comms to multiple nodes is managed by using node fields in request command
 each node will silently discards request if specified node doesn't match it's own
 master is always node 0, and if node not specified in request then node 0 is assumed
 if request specifies destination node, then response includes node field
 
 History
 Sep14 - draft version, float variable type not supported due to Arduino limited resources - and really, how often do you absolutely need floats anyway
 Apr 15 - reserve EEPROM space for tftp bootloader network settings, update settings using sysvar
 
 */
#include <EEPROM.h>    // also needs EEPROM.h included in sketch
#include <stdio.h>
#include <string.h>

#include "ard_sysvar.h"

#define EEPROM_SIZE  1000

#define BOOTLOADER_EEPROM_SIZE  30
#define SYSVAR_SECTION_START  BOOTLOADER_EEPROM_SIZE

#define SYSVAR_SIZE  (1+MAX_STRING_SIZE)


/*****************************************
  public variables
*/
VarRecord_t  globalVarRecord;

/*****************************************
private variables
*/
uint8_t NodeId;

/*************
  tftp bootloader network settings
  [0x55][0xAA]    // signature, if present bootloader will use these settings
  [ImageStatus]    // 0xFF = bad or not loaded, 0xBB = good
  [GW0][GW1][GW2][GW3]  // gateway 
  [MASK0][MASK1][MASK2][MASK3]  //subnet mask
  [MAC0][MAC1][MAC2][MAC3][MAC4][MAC5] // mac address
  [IP0][IP1][IP2][IP3]  // device ip address
*/
typedef struct {
	uint8_t signature[2];
  uint8_t imageFlag;
  uint8_t gate[4];
  uint8_t sub[4];
  uint8_t mac[6];
  uint8_t ip[4];        
} BootSettings_t;

typedef union {
        uint8_t arr[BOOTLOADER_EEPROM_SIZE];
        BootSettings_t settings;
} BootRecord_t;

BootRecord_t BootloaderSettings = {0x55,0xAA,0xBB,192,168,1,1,255,255,255,0,0x12,0x34,0x56,0x78,0x9A,0xBC,192,168,1,110};

/*****************************************
  private function prototypes
*/
channel_rd_func channel_read;
channel_wr_func channel_write;

int err_response(char* pBuff, uint8 err_code, const char* cmd_str);
char* token(char* str, const char* delimiter);


/*****************************************
 protocol handler
 simplified version of MUX protocol, only supports get & set of variables (type 'v') & channels ('c')
 
 ASCII text based protocol, cmd<SP>param1<SP>param2 or data<SP>param3 or data<CR>
 - all cmds terminate with CR, 
 - all response lines terminate with CR, some response may be multiple lines
 - param field has single char type then number with no seperator
 - data is interpreted based on param type, either discrete, decimal(integer)or string
  
 input: buffer with input string
 
 returns:
 input buffer is modified to contain the response, 
 returns >0 if response in buffer to be sent
*/

#define LIMIT_MAX_VALUE(_x, _max)  _x = (_x <= _max) ? _x : _max  

int processCmd(char* pBuff)
{
  char type=0;
  char* p;
  int i, node, index_start,index_end;
  
  p = token(pBuff," \r");
  if(!p) return 0;
  
  node = -1;  
  index_start = -1;
  index_end = -1;
  
  if(strcmp(p,"get") == 0) {	// "get" command can take multiple params
    do {
      p = token(NULL," \r");
      if(p) {    
        if(p[0] == 'n') {  // node specifier
          sscanf(p,"%*c%d",&node);
        }
        else {
          if(!type) type = p[0];
          sscanf(p,"%*c%d",&i);
          if(index_start < 0) {
            index_start = i;
          }
          else if((index_end < 0) && (p[0] == type)) {
            index_end = i;
          }
        }
      }
    } while(p) ;
    
    if(index_start >= 0) {
      if(index_end < index_start)  index_end = index_start;
      if((index_end - index_start) >= MAX_LINES_PER_RESPONSE) index_end = index_start + MAX_LINES_PER_RESPONSE - 1;
      pBuff[0] = '\0';
      p = pBuff;
      // ignore node param unless explicitly stated
      // if not matching node, then silently discard request
      if(((node < 0) && (NodeId == 0)) || (node == NodeId)) {
        switch(type) {
  	case 'c': LIMIT_MAX_VALUE(index_start, NUM_CHANNELS-1);
                  LIMIT_MAX_VALUE(index_end, NUM_CHANNELS-1);
                  for(i = index_start; i <= index_end; i++) {
                    if(node >= 0) p += sprintf(p,"n%d ", node);
  		    p += channel_print(i, p);
  		  }
         	  break;
  				
  	case 'v': LIMIT_MAX_VALUE(index_start, NUM_SYSVARS-1);
                  LIMIT_MAX_VALUE(index_end, NUM_SYSVARS-1);
                    for(i = index_start; i <= index_end; i++) {
                      if(node >= 0) p += sprintf(p,"n%d ", node);
  		      p += sysvar_print(i,p);
  		}
  		break;
  				
  	default: err_response(pBuff, ERR_INVALID_TYPE, "get");
  		break;
  	}
      }
    }
    else {
      err_response(pBuff, ERR_PARAM_MISSING, "get");
    }
  }  // end get command
  else if(strcmp(p,"set") == 0) {   // "set" command takes single param + data
    p = token(NULL," \r");
    if(p) {  // 1st param
      if(p[0] == 'n') {  // optional node specifier
        sscanf(p,"%*c%d",&node);
        p = token(NULL," \r");
      }
      if(p) sscanf(p,"%c%d",&type,&index_start);
      p = token(NULL,"\r");   //start of data
      if(p) {
        pBuff[0] = '\0';  // empty response buffer, but leave data, pointed to by p, intact
        if(((node < 0) && (NodeId == 0)) || (node == NodeId)) {
          switch(type) {
            case 'c': channel_scan(index_start, p);
                      p = pBuff;
                      if(node >= 0) p += sprintf(p,"n%d ", node);
          	      channel_print(index_start, p);
        	    break;
        
            case 'v':  sysvar_scan(index_start, p);
                      p = pBuff;
                      if(node >= 0) p += sprintf(p,"n%d ", node);
                      sysvar_print(index_start, p);
        	  break;
        						
            default: err_response(pBuff, ERR_INVALID_TYPE, "set");
        	break;
          }
        }
      }
      else { // no data
         err_response(pBuff, ERR_PARAM_MISSING, "set");
      }
    }
    else {
      err_response(pBuff, ERR_PARAM_MISSING, "set");
    }
  }  // end set command
  else {
    err_response(pBuff, ERR_INVALID_CMD, NULL);
  }
  
  return strlen(pBuff);
}



/********************************************
 * error response, error code, and command responsible
 * format:
 * 		err<SP>[code]<SP>[command]<CR>
 *
 * 	returns num of bytes in response
 */
int err_response(char* pBuff, uint8 err_code, const char* cmd_str)
{
  return (cmd_str != NULL) ? sprintf(pBuff,"err %02d %s\r", err_code, cmd_str):sprintf(pBuff,"err %02d\r",err_code);
}



/*******************************************************
system variable handling
*/

void sysvar_init(void)
{
  int i,x;
  VarRecord_t VarRecord;
  
  if((sysvar_read(SYSVAR_DB_VER_IDX)->i != SYSVAR_DB_VERSION) 
		|| (sysvar_read(SYSVAR_DB_NUM_IDX)->i != NUM_SYSVARS)) {
    for(i=0; i < NUM_SYSVARS; i++) {
      VarRecord.type = sysvarDefaults[i].type;
      for(x=0; x < MAX_STRING_SIZE; x++) {
        VarRecord.c[x] = sysvarDefaults[i].c[x];
      }
      sysvar_write(i, &VarRecord);
    }    
  }
  
  NodeId = sysvar_read(SYSVAR_NODE_IDX)->i;
  
  channel_read = NULL;
  channel_write = NULL;
  
}


int sysvar_print(uint16 idx, char* pBuff)
{
  int len=0;
  
   switch(sysvar_read(idx)->type) {
      case sysvar_discrete:
      case sysvar_decimal:   len = sprintf(pBuff,"v%d %d %d\r", idx, sysvar_read(idx)->type, sysvar_read(idx)->i); 
                              break;
      case sysvar_string:    len = sprintf(pBuff,"v%d %d %s\r", idx, sysvar_string, sysvar_read(idx)->c); break;
      default:               len = sprintf(pBuff,"v%d %d NA\r", idx, sysvar_none); break;
    }
  
  return len;
}


int sysvar_scan(uint16 idx, char* pBuff)
{
    int i;  
    VarRecord_t VarRecord;
    
    VarRecord.type = sysvar_read(idx)->type;
    switch(VarRecord.type) {
      case sysvar_discrete: VarRecord.i = (pBuff[0] == '1') ? 1:0; break;
      case sysvar_decimal: sscanf(pBuff,"%d", &VarRecord.i); break;
      case sysvar_string:  i=0;
                           while((i < MAX_STRING_SIZE) && (pBuff[i] != '\r') && (pBuff[i] != '\0')) {
                             VarRecord.c[i] = pBuff[i];
                             ++i;
                             if(i < MAX_STRING_SIZE) VarRecord.c[i] = 0;
                           }
                           break;
      default:    VarRecord.type = sysvar_none; break;
    }
    if((idx > TOP_READ_ONLY_SYSVAR) && (VarRecord.type != sysvar_none))
        sysvar_write(idx, &VarRecord);
        
   return 1;
}


VarRecord_t* sysvar_read(uint16 idx)
{
  int i;
  
  globalVarRecord.type = sysvar_none;
  globalVarRecord.i = 0;

  if(idx < NUM_SYSVARS) {
    idx = SYSVAR_SECTION_START + (idx * SYSVAR_SIZE);
    if(idx < (EEPROM_SIZE - SYSVAR_SIZE)) {
      globalVarRecord.type = EEPROM.read(idx);
      ++idx;
      for(i = 0; i < MAX_STRING_SIZE; i++) {
        globalVarRecord.c[i] = EEPROM.read(idx);
        ++idx;
      } 
    }    
  }
  
  return &globalVarRecord; 
}


int sysvar_write(uint16 idx, VarRecord_t* VarRecord)
{
  int i, result = 0;
  
  if(idx < NUM_SYSVARS) {
    idx = SYSVAR_SECTION_START + (idx * SYSVAR_SIZE);
    if(idx < (EEPROM_SIZE - SYSVAR_SIZE)) {
      if(EEPROM.read(idx) != VarRecord->type)  EEPROM.write(idx, VarRecord->type);
      ++idx;
      for(i = 0; i < MAX_STRING_SIZE; i++) {
        if(EEPROM.read(idx) != VarRecord->c[i]) EEPROM.write(idx, VarRecord->c[i]);
        ++idx;
      } 
      result = 1;     
    }
  }
  
  return result; 
}


/*******************************************************
channel scan and print, use same data structure as system variables
*/

int channel_print(uint16 idx, char* pBuff)
{
  int len=0;
  
   if(channel_read == NULL) return 0;
  
   switch(channel_read(idx)->type) {
      case sysvar_discrete:  
      case sysvar_decimal:   len = sprintf(pBuff,"c%d %d %d\r", idx, channel_read(idx)->type, channel_read(idx)->i); 
                              break;
      case sysvar_string:    len = sprintf(pBuff,"c%d %d %s\r", idx, sysvar_string, channel_read(idx)->c); break;
      default:               len = sprintf(pBuff,"c%d %d NA\r", idx, sysvar_none); break;
    }
   
   return len;
}


int channel_scan(uint16 idx, char* pBuff)
{
    int i;  
    VarRecord_t VarRecord;
    
    if((channel_read == NULL) || (channel_write == NULL)) return 0;
    
    VarRecord.type = channel_read(idx)->type;
    switch(VarRecord.type) {
      case sysvar_discrete: VarRecord.i = (pBuff[0] == '1') ? 1:0; break;
      case sysvar_decimal: sscanf(pBuff,"%d", &VarRecord.i); break;
      case sysvar_string:  i=0;
                           while((i < MAX_STRING_SIZE) && (pBuff[i] != '\r') && (pBuff[i] != '\0')) {
                             VarRecord.c[i] = pBuff[i];
			     ++i;
                             if(i < MAX_STRING_SIZE) VarRecord.c[i] = 0;
                           }
                           break;
      default:    VarRecord.type = sysvar_none; break;
    }
    if(VarRecord.type != sysvar_none) 
        channel_write(idx, &VarRecord);
        
   return 1;
}


void attach_channel_read(channel_rd_func rd_func)
{
  channel_read = rd_func;
}

void attach_channel_write(channel_wr_func wr_func)
{
  channel_write = wr_func;
}



/*********************
*  string to array helper function

  input 
  ptr to source C string (null terminated char array)
  ptr to destination uint8 (unsigned char) array
  if isHex is true then treats string as hex inputs
  
  output
  number of array elements saved into destination
*/
int strToArray(char* str, uint8* arr, uint8 isHex)
{
  char *p;
  int i, numBytes = 0;
    
  do {
    p = token(str, ",.:- ");
    if(p) {
      i = (isHex) ? sscanf(p,"%X",&arr[numBytes]) : sscanf(p,"%d",&arr[numBytes]);   
      ++numBytes;
    }
    str = NULL;
  } while(p) ;
  
  return numBytes;
}

/*****************************************************
 * tokenise string, same as strtok function but with less impact on source string
 * only modification to source is to replace 1st separator with \0 to terminate each token
 *
 * this function will return the last token before NULL terminator as a valid token
 *
 * if input ptr != NULL then search from start, if NULL search from last known position
 * ignores 0 length tokens
 * returns ptr to token, or NULL if not found
 */
char* token(char* str, const char* delimiter)
{
	int x, found;
	char* result;
	static char* ptr = NULL;

	if(str != NULL)	{	// reset to beginning
          ptr = str;
        }

        if(*ptr == 0) return NULL;
        
	result = ptr;
	found = 0;
        do {
		for(x=0; delimiter[x] != 0; x++) {
		  if(*ptr == delimiter[x]) {
			found = 1;
			*ptr = 0;
		  }
		}
                ++ptr;          
        } while((*ptr != 0) && (!found));
        
        // if null terminator found, return valid token without incrementing ptr
        // next pass will fail since we're still pointing to null terminator
        if(*ptr==0) found = 1;
        
	// skip over multiple delimiting chars, ready for next search
	while((found==1) && (*ptr != 0)) {
		for(x=0; delimiter[x] != 0; x++) {
			if(*ptr != delimiter[x]) {
				found = 2;
			}
		}
		if(found < 2) ++ptr;
	}

	return (found) ? result:NULL;
}

