/***************************************************************
 system variables for arduino target - needs EEPROM.h in sketch
 
 reduced function compared to full implementation on MUX
 
 supports discrete, decimal (integer) and string types
*/
#ifndef ARD_SYSVAR_H
#define ARD_SYSVAR_H

enum sysvar_type {	sysvar_none,
			              sysvar_discrete,
			              sysvar_decimal,
			              sysvar_string };

#define SYSVAR_DB_VERSION  4
#define SYSVAR_HW_VERSION  1
#define SYSVAR_SW_VERSION  1

#define MAX_STRING_SIZE          20
#define NUM_CHANNELS            100

// device node in multi drop networks
#define DEFAULT_NODE_ID    0

enum sysvar_index { SYSVAR_DB_NUM_IDX = 0,
                    SYSVAR_DB_VER_IDX,
                    SYSVAR_HW_VER_IDX,
                    SYSVAR_SW_VER_IDX,
                    SYSVAR_NODE_IDX,
                    SYSVAR_ERR_LIMIT_IDX,
                    SYSVAR_SUN_THRESH_IDX,
                    SYSVAR_CLOUD_THRESH_IDX,
                    NUM_SYSVARS };
                    
#define TOP_READ_ONLY_SYSVAR  SYSVAR_SW_VER_IDX

typedef struct {
  unsigned char type;
  union{
    char c[MAX_STRING_SIZE];
    int i;
    float f;  // don't use, not working yet
  } ;
} VarRecord_t;

const VarRecord_t sysvarDefaults[NUM_SYSVARS] = {
                  {sysvar_decimal, {i:NUM_SYSVARS}},
                  {sysvar_decimal, {i:SYSVAR_DB_VERSION}},
                  {sysvar_decimal, {i:SYSVAR_HW_VERSION}},
                  {sysvar_decimal, {i:SYSVAR_SW_VERSION}},
                  {sysvar_decimal, {i:DEFAULT_NODE_ID}},
                  {sysvar_decimal, {i:20}},
                  {sysvar_decimal, {i:100}},
                  {sysvar_decimal, {i:3}}
                  };


// determine size of response, 
// must manually select so as not to overrun buffer
#define MAX_LINES_PER_RESPONSE  4    

enum err_type { ERR_INVALID_CMD=1,
                ERR_OUT_OF_RANGE,
                ERR_PARAM_MISSING,
                ERR_INVALID_TYPE };
			
extern VarRecord_t  globalVarRecord;

#ifndef uint8      
typedef unsigned char uint8;
#endif

#ifndef uint16      
typedef unsigned short uint16;
#endif

// function prototypes
void sysvar_init(void);

// main entry to library
int processCmd(char* pBuff);

// helper function for system vars as strings
int strToArray(char* str, uint8* arr, uint8 isHex);

// systyem variable functions
VarRecord_t* sysvar_read(uint16 idx);
int sysvar_write(uint16 idx, VarRecord_t* VarRecord);
int sysvar_print(uint16 idx, char* pBuff);
int sysvar_scan(uint16 idx, char* pBuff);


/****************************
 io channel read/write provided by external functions
 function ptrs are provided to attach external functions that are called as required by protocol handler
 */
typedef VarRecord_t* (*channel_rd_func)(uint16 idx);
typedef int (*channel_wr_func)(uint16 idx, VarRecord_t* VarRecord);

int channel_print(uint16 idx, char* pBuff);
int channel_scan(uint16 idx, char* pBuff);

void attach_channel_read(channel_rd_func rd_func);
void attach_channel_write(channel_wr_func wr_func);


#endif
// end file
